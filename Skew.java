import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Vector;
import ij.plugin.frame.*;
import ij.*;
import ij.process.*;
import ij.gui.*;
import java.awt.image.BufferedImage;

/**
 A simple port of a brute force skew algorithm.
 */
public class Skew extends PlugInFrame implements ActionListener {
    
	Panel panel;
	int previousID;
	static Frame instance;
    Choice skewType;
    
    Label statusLabel;
    
	public Skew() {
		super("Skew");
		if (IJ.versionLessThan("1.39t"))
			return;
		if (instance!=null) {
			instance.toFront();
			return;
		}
		instance = this;
		addKeyListener(IJ.getInstance());
        
		setLayout(new FlowLayout());
		panel = new Panel();
		panel.setLayout(new GridLayout(4, 1, 5, 5));
        
        Label label = new Label("Skew search type:");
        panel.add(label);
        
        skewType = new Choice();
        skewType.add("Positive");
        skewType.add("Negative");
        skewType.add("Both");
        
        panel.add(skewType);
        
        addButton("Go");
        
        statusLabel = new Label("Idle");
        statusLabel.setAlignment(Label.CENTER);
        panel.add(statusLabel);
        
		add(panel);
		
		pack();
		GUI.center(this);
        setVisible(true);
	}
	/**
     * Add a simple button to the UI.
     */
	void addButton(String label) {
		Button b = new Button(label);
		b.addActionListener(this);
		b.addKeyListener(IJ.getInstance());
		panel.add(b);
	}
    
    /**
     * Method called when the button is present.
     */
	public void actionPerformed(ActionEvent e) {
		ImagePlus imp = WindowManager.getCurrentImage();
        //make sure we actually do an operation on image.
		if (imp==null) {
			IJ.beep();
			IJ.showStatus("No image");
			previousID = 0;
			return;
		}
		if (!imp.lock())
        {previousID = 0; return;}
		ImageProcessor ip = imp.getProcessor();
		int id = imp.getID();
		if (id!=previousID)
			ip.snapshot();
		previousID = id;
        String label = e.getActionCommand();
        String skew = skewType.getSelectedItem();
		new DeskewerRunner(label, skew, imp, ip, statusLabel);
	}
    
	public void processWindowEvent(WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID()==WindowEvent.WINDOW_CLOSING) {
			instance = null;	
		}
	}
}

//---------------------------------------------------------------
/**
 * Class represents simple image skew detector based on ray cats.
 */
class SkewDetector {
    String skewType;
    ImagePlus imagePlus;
    BufferedImage bufferedImage;
    float angleStep;
    int lineStep;
    ImageProcessor ip;
    Vector<RayHit> rayHits;
    
	/**
	 * Class contructor with initialize parameters.
	 * @param	skew is .
	 * @param	imp is .
	 * @param	processor is .
	 */
    SkewDetector(String skew, ImagePlus imp, ImageProcessor processor) {
        this.skewType = skew;
        this.imagePlus = imp;
        this.angleStep = 1.0f;
        this.lineStep = 10;
        this.ip = processor;
        this.rayHits = new Vector<RayHit>();
        this.bufferedImage = imp.getBufferedImage();
    }
    
	/**
	 * Private method which is used to get pixel value from image.
	 * @param	img is pointer to image representaion.
	 * @param	x is x coord.
	 * @param	y is y coord.
	 * @return	pixel value, 0 if white, 1 if black.
	 */
    private int getPixel(BufferedImage img, int x, int y) {
        if(x < 0 || x >= img.getWidth() || y < 0 || y >= img.getHeight()) {
            return 0;
        }
        int pixelData = img.getRGB(x,y);
        
        int red = (pixelData >> 16) & 0x000000FF;
        int green = (pixelData >>8 ) & 0x000000FF;
        int blue = (pixelData) & 0x000000FF;
        
        int greyValue = (int)(red + green + blue)/3;
        
        int isWhitePixel = 0;
        if (greyValue < 127) {
            isWhitePixel = 1;
        }
        return isWhitePixel;
	}
    
	/**
	 * Private method which is used to search negative skew.
	 */
    private void searchNegativeSkew() {
        for(int i = 0; i < imagePlus.getHeight(); i += lineStep)
		{
			for(float angle = 0.0f; angle < 90.0f; angle += angleStep)
			{
                //calucate the angle
				float a = (float)Math.tan(angle*Math.PI/180.0f);
				int hits = 0;
                
                //count the hits
				for(int x = 0; x < imagePlus.getWidth(); ++x) {
					hits += getPixel(bufferedImage, x, imagePlus.getHeight() - 1 - (int)(a*x+i));
                }
                
                //push back results
                RayHit rayHit = new RayHit(-angle, hits);
                rayHits.add(rayHit);
            }
		}
    }
    
	/**
	 * Private method which is used to search positive skew.
	 */
    private void searchPositiveSkew() {
        for(int i = 0; i < imagePlus.getHeight(); i += lineStep)
		{
			for(float angle = 1.0f; angle < 90.0f; angle += angleStep)
			{
                float a = (float)Math.tan(angle*Math.PI/180.0f);
				int hits = 0;
                
				for(int x = 0; x < imagePlus.getWidth(); ++x) {
					hits += getPixel(bufferedImage, x, (int)(a*x+i));
                }
                
                RayHit rayHit = new RayHit(angle, hits);
                rayHit.hitHeight = i;
                rayHits.add(rayHit);
			}
		}
    }
    
	/**
	 * Method is used to calculate skew of selected image. Use simple ray hits algorithm.
	 * @return	skew angle value.
	 */
    public float calculateSkew() {
        if (skewType.equals("Positive")) {
            searchPositiveSkew();
        }
        else if (skewType.equals("Negative")) {
            searchNegativeSkew();
        }
        else {
            searchPositiveSkew();
            searchNegativeSkew();
        }
        
        float skew = 0;
        int maxHits = 0;
        
        //get the angle with most hits
        
        for(RayHit rayHit : rayHits) {
        
            if (rayHit.hits > maxHits) {
                skew = rayHit.angle;
                IJ.showStatus("Angle: " + rayHit.angle + " hits: " + rayHit.hits + " hit height: " + rayHit.hitHeight);
                maxHits = rayHit.hits;

            }
        }
        
        return skew;
    }
}

//---------------------------------------------------------------
/**
 * Simple container class.
 */
class RayHit {
    public float angle;
    public int hits;
    
    public int hitHeight;
    
    public RayHit(float angle, int hits) {
        this.angle = angle;
        this.hits = hits;
    }
}

//---------------------------------------------------------------
/**
 * Class represents the thread of the program.
 */
class DeskewerRunner extends Thread {
	private String command;
	private ImagePlus imp;
	private ImageProcessor ip;
    private String skewType;
    private Label statusLabel;
    
	DeskewerRunner(String command, String skew, ImagePlus imp, ImageProcessor ip, Label statusLabel) {
		super(command);
		this.command = command;
		this.imp = imp;
		this.ip = ip;
        this.skewType = skew;
        this.statusLabel = statusLabel;
		setPriority(Math.max(getPriority()-2, MIN_PRIORITY));
		start();
	}
    
	public void run() {
		try {runCommand(command, imp, ip);}
		catch(OutOfMemoryError e) {
			IJ.outOfMemory(command);
			if (imp!=null) imp.unlock();
		} catch(Exception e) {
			CharArrayWriter caw = new CharArrayWriter();
			PrintWriter pw = new PrintWriter(caw);
			e.printStackTrace(pw);
			IJ.write(caw.toString());
			IJ.showStatus("");
			if (imp!=null) imp.unlock();
		}
	}
    
	private void runCommand(String command, ImagePlus imp, ImageProcessor ip) {
		IJ.showStatus(command + "...");
		Roi roi = imp.getRoi();
        
        statusLabel.setText("Calculating skew...");
        
        SkewDetector skewDetector = new SkewDetector(skewType, imp, ip);
        float skewAngle = skewDetector.calculateSkew();
        
        statusLabel.setText("Calculated skew is: " + skewAngle);
        
        ip.rotate(-skewAngle);
        
		imp.updateAndDraw();
		imp.unlock();
		IJ.showStatus("Done!");
	}
}
